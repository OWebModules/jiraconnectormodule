﻿using Atlassian.Jira;
using JiraConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<SyncGitCommentsModelResult>> GetSyncGitCommentsModel(SyncGitCommentsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncGitCommentsModelResult>>(() =>
            {
                var result = new ResultItem<SyncGitCommentsModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new SyncGitCommentsModelResult()
                };

                result.ResultState = Validation.ValidationController.ValidateSyncGitCommentsRequest(request, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = GitCommentSync.Config.LocalData.Class_Jira_Git_Comment_Sync.GUID
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the config!";
                    return result;
                }

                result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

                if (result.Result.Config == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Config cannot be found!";
                    return result;
                }

                var searchJiraConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GitCommentSync.Config.LocalData.ClassRel_Jira_Git_Comment_Sync_belongs_to_JiraConnectorModule.ID_RelationType,
                        ID_Parent_Other = GitCommentSync.Config.LocalData.ClassRel_Jira_Git_Comment_Sync_belongs_to_JiraConnectorModule.ID_Class_Right
                    }
                };

                var dbReaderJiraConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderJiraConfig.GetDataObjectRel(searchJiraConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Jira-Config!";
                    return result;
                }

                result.Result.JiraConfig = dbReaderJiraConfig.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.JiraConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Jira-Config cannot be found!";
                    return result;
                }

                var searchGitConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GitCommentSync.Config.LocalData.ClassRel_Jira_Git_Comment_Sync_belongs_to_GitConnectorModule.ID_RelationType,
                        ID_Parent_Other = GitCommentSync.Config.LocalData.ClassRel_Jira_Git_Comment_Sync_belongs_to_GitConnectorModule.ID_Class_Right
                    }
                };

                var dbReaderGitConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGitConfig.GetDataObjectRel(searchGitConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the git-config!";
                    return result;
                }

                var searchRegex = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GitCommentSync.Config.LocalData.ClassRel_Jira_Git_Comment_Sync_Issue_Recognition_Regular_Expressions.ID_RelationType,
                        ID_Parent_Other = GitCommentSync.Config.LocalData.ClassRel_Jira_Git_Comment_Sync_Issue_Recognition_Regular_Expressions.ID_Class_Right
                    }
                };

                var dbReaderRegex = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRegex.GetDataObjectRel(searchRegex);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the RegEx OItem!";
                    return result;
                }

                result.Result.RegEx = dbReaderRegex.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.RegEx == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No RegEx found!";
                    return result;
                }

                var searchRegexAttribute = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.RegEx.GUID,
                        ID_AttributeType = GitCommentSync.Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_AttributeType
                    }
                };

                var dbReaderRegexAttribute = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRegexAttribute.GetDataObjectAtt(searchRegexAttribute);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the RegEx Attribute!";
                    return result;
                }

                result.Result.RegExAttribute = dbReaderRegexAttribute.ObjAtts.FirstOrDefault();

                if (result.Result.RegExAttribute == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No RegEx attribute found!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjects(List<string> objectNames, string idClass, GetSyncIssuesModelResult modelResult)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                if (!objectNames.Any()) return result;

                var searchTaskIds = objectNames.Select(ident => new clsOntologyItem
                {
                    Name = ident,
                    GUID_Parent = idClass
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);
                var dbWriter = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(searchTaskIds);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Task-Ids!";
                    return result;
                }


                result.Result = (from obj in dbReader.Objects1
                                 join objName in objectNames on obj.Name.ToLower() equals objName.ToLower()
                                 select obj).ToList();

                var taskIdsToSave = (from ident in objectNames
                                     join taskId in dbReader.Objects1 on ident.ToLower() equals taskId.Name.ToLower() into taskIds
                                     from taskId in taskIds.DefaultIfEmpty()
                                     where taskId == null
                                     select new clsOntologyItem
                                     {
                                         GUID = globals.NewGUID,
                                         Name = ident,
                                         GUID_Parent = idClass,
                                         Type = globals.Type_Object
                                     }).ToList();

                if (taskIdsToSave.Any())
                {
                    result.ResultState = dbWriter.SaveObjects(taskIdsToSave);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving new task-ids";
                        return result;
                    }

                    result.Result.AddRange(taskIdsToSave);
                }
                
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> CheckIssuesOfIdentifiers(List<clsOntologyItem> jiraIdentifiersWithAdditionalIssueName, GetSyncIssuesModelResult modelResult)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                if (!jiraIdentifiersWithAdditionalIssueName.Any()) return result;

                var searchIssues = new List<clsObjectRel>();

                if (modelResult.DirectionIssueTaskId.GUID == globals.Direction_LeftRight.GUID)
                {
                    searchIssues = jiraIdentifiersWithAdditionalIssueName.Select(ident => new clsObjectRel
                    {
                        ID_Other = ident.GUID,
                        ID_RelationType = modelResult.RelationTypeIssueTaskId.GUID,
                        ID_Parent_Object = modelResult.IssueClass.GUID
                    }).ToList();
                }
                else
                {
                    searchIssues = jiraIdentifiersWithAdditionalIssueName.Select(ident => new clsObjectRel
                    {
                        ID_Object = ident.GUID,
                        ID_RelationType = modelResult.RelationTypeIssueTaskId.GUID,
                        ID_Parent_Other = modelResult.IssueClass.GUID
                    }).ToList();
                }

                var dbReaderIssues = new OntologyModDBConnector(globals);
                var dbWriterIssues = new OntologyModDBConnector(globals);

                if (searchIssues.Any())
                {
                    result.ResultState = dbReaderIssues.GetDataObjectRel(searchIssues);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the issues";
                        return result;
                    }
                }

                var relationConfig = new clsRelationConfig(globals);
                var issuesToSave = new List<clsOntologyItem>();
                var relationsToSave = new List<clsObjectRel>();

                if (modelResult.DirectionIssueTaskId.GUID == globals.Direction_LeftRight.GUID)
                {
                    var issuesToSavePre = (from ident in jiraIdentifiersWithAdditionalIssueName
                                           join rel in dbReaderIssues.ObjectRels on ident.GUID equals rel.ID_Other into rels
                                           from rel in rels.DefaultIfEmpty()
                                           select new { ident, rel });

                    issuesToSavePre.ToList().ForEach(pre =>
                    {
                        clsOntologyItem issueToSave = null;
                        clsObjectRel issueToTaskId = null;
                        if (pre.rel == null)
                        {
                            issueToSave = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = pre.ident.Additional1,
                                GUID_Parent = modelResult.IssueClass.GUID,
                                Type = globals.Type_Object
                            };

                            issueToTaskId = relationConfig.Rel_ObjectRelation(issueToSave, pre.ident, modelResult.RelationTypeIssueTaskId);
                        }
                        else if (pre.rel.Name_Object != pre.ident.Additional1)
                        {
                            issueToSave = new clsOntologyItem
                            {
                                GUID = pre.rel.ID_Object,
                                Name = pre.ident.Additional1,
                                GUID_Parent = modelResult.IssueClass.GUID,
                                Type = globals.Type_Object
                            };
                        }

                        if (issueToSave != null)
                        {
                            issuesToSave.Add(issueToSave);
                        }

                        if (issueToTaskId != null)
                        {
                            relationsToSave.Add(issueToTaskId);
                        }
                    });
                }
                else
                {
                    var issuesToSavePre = (from ident in jiraIdentifiersWithAdditionalIssueName
                                           join rel in dbReaderIssues.ObjectRels on ident.GUID equals rel.ID_Object into rels
                                           from rel in rels.DefaultIfEmpty()
                                           select new { ident, rel });

                    issuesToSavePre.ToList().ForEach(pre =>
                    {
                        clsOntologyItem issueToSave = null;
                        clsObjectRel issueToTaskId = null;
                        if (pre.rel == null)
                        {
                            issueToSave = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = pre.ident.Additional1,
                                GUID_Parent = modelResult.IssueClass.GUID,
                                Type = globals.Type_Object
                            };

                            issueToTaskId = relationConfig.Rel_ObjectRelation(pre.ident, issueToSave, modelResult.RelationTypeIssueTaskId);
                        }
                        else if (pre.rel.Name_Other != pre.ident.Additional1)
                        {
                            issueToSave = new clsOntologyItem
                            {
                                GUID = pre.rel.ID_Object,
                                Name = pre.ident.Additional1,
                                GUID_Parent = modelResult.IssueClass.GUID,
                                Type = globals.Type_Object
                            };
                        }

                        if (issueToSave != null)
                        {
                            issuesToSave.Add(issueToSave);
                        }

                        if (issueToTaskId != null)
                        {
                            relationsToSave.Add(issueToTaskId);
                        }
                    });
                }
                
                if (issuesToSave.Any())
                {
                    result.ResultState = dbWriterIssues.SaveObjects(issuesToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while writing Issues!";
                        return result;
                    }
                }

                if (relationsToSave.Any())
                {
                    result.ResultState = dbWriterIssues.SaveObjRel(relationsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while writing Issue to Task-Id relations!";
                        return result;
                    }
                }

                result.Result = dbReaderIssues.ObjectRels;
                result.Result.AddRange(relationsToSave);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetIssuesResult>> GetIssues(List<Issue> jiraIssues, string idClassTaskId, string idClassIssues, string idRelationType, string idDirectionIssueToTaskId)
        {
            var taskResult = await Task.Run<ResultItem<GetIssuesResult>>(() =>
           {
               var result = new ResultItem<GetIssuesResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetIssuesResult()
               };

               var searchTaskIds = jiraIssues.Select(issue => new clsOntologyItem
               {
                   Name = issue.JiraIdentifier,
                   GUID_Parent = idClassTaskId
               }).ToList();

               if (!searchTaskIds.Any())
               {
                   return result;
               }

               var dbReaderTaskIds = new OntologyModDBConnector(globals);
               var dbReaderIssues = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTaskIds.GetDataObjects(searchTaskIds);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Task-Ids!";
                   return result;
               }

               result.Result.TaskIds = dbReaderTaskIds.Objects1;

               var searchIssues = new List<clsObjectRel>();
               if (idDirectionIssueToTaskId == globals.Direction_LeftRight.GUID)
               {
                   searchIssues = result.Result.TaskIds.Select(taskId => new clsObjectRel
                   {
                       ID_Other = taskId.GUID,
                       ID_RelationType = idRelationType,
                       ID_Parent_Object = idClassIssues
                   }).ToList();
               }
               else
               {
                   searchIssues = result.Result.TaskIds.Select(taskId => new clsObjectRel
                   {
                       ID_Object = taskId.GUID,
                       ID_RelationType = idRelationType,
                       ID_Parent_Other = idClassIssues
                   }).ToList();
               }
               
               if (searchIssues.Any())
               {
                   result.ResultState = dbReaderIssues.GetDataObjectRel(searchIssues);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the issues!";
                       return result;
                   }

                   result.Result.Issues = dbReaderIssues.ObjectRels;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> CheckRelations(List<clsObjectRel> relations, string relationClass, string idDirection, bool deleteOldRelations = false)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                if (!relations.Any()) return result;

                var relationConfig = new clsRelationConfig(globals);

                var searchRealtions = relations.Select(issue => new clsObjectRel
                {
                    ID_Object = idDirection == globals.Direction_LeftRight.GUID ? issue.ID_Object : null,
                    ID_Other = idDirection == globals.Direction_LeftRight.GUID ? null : issue.ID_Other,
                    ID_Parent_Other = idDirection == globals.Direction_LeftRight.GUID ? relationClass : null,
                    ID_Parent_Object = idDirection == globals.Direction_LeftRight.GUID ? null : relationClass,
                    ID_RelationType = issue.ID_RelationType
                }).ToList();

                var dbReaderRelations = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRelations.GetDataObjectRel(searchRealtions);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relations between Issues and States";
                    return result;
                }


                if (!deleteOldRelations)
                {
                    
                    var newRelations = (from issueState in relations
                                        join rel in dbReaderRelations.ObjectRels on new { issueState.ID_Object, issueState.ID_Other, issueState.ID_RelationType, issueState.OrderID } equals new { rel.ID_Object, rel.ID_Other, rel.ID_RelationType, rel.OrderID } into rels
                                        from rel in rels.DefaultIfEmpty()
                                        where rel == null
                                        select issueState).ToList();

                    if (newRelations.Any())
                    {
                        var dbWriter = new OntologyModDBConnector(globals);
                        result.ResultState = dbWriter.SaveObjRel(newRelations);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the state-relations";
                            return result;
                        }
                    }

                    result.Result = dbReaderRelations.ObjectRels;
                    result.Result.AddRange(newRelations);
                }
                else
                {
                    var dbWriter = new OntologyModDBConnector(globals);
                    if (dbReaderRelations.ObjectRels.Any())
                    {
                        var deleteItems = dbReaderRelations.ObjectRels.Select(rel => new clsObjectRel
                        {
                            ID_Object = rel.ID_Object,
                            ID_RelationType = rel.ID_RelationType,
                            ID_Other = rel.ID_Other
                        }).ToList();


                        if (deleteItems.Any())
                        {
                            result.ResultState = dbWriter.DelObjectRels(deleteItems);
                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while deleting old state-relations";
                                return result;
                            }
                        }
                    }

                    if (relations.Any())
                    {
                        

                        result.ResultState = dbWriter.SaveObjRel(relations);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving state-relations";
                            return result;
                        }
                    }

                }

               
                                    
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckStates(List<string> states, GetSyncIssuesModelResult modelResult)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchStates = states.Select(state => new clsOntologyItem
                {
                    GUID_Parent = modelResult.StatesClass.GUID,
                    Name = state
                }).ToList();

                var dbReaderStates = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderStates.GetDataObjects(searchStates);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the states";
                    return result;
                }

                var dbStates = new List<clsOntologyItem>();

                var statesPre = (from state in states
                                 join dbState in dbReaderStates.Objects1 on state.ToLower() equals dbState.Name.ToLower() into dbStates1
                                 from dbState in dbStates1.DefaultIfEmpty()
                                 select new { state, dbState }).ToList();

                statesPre.ForEach(dbState =>
                {
                    if (dbState.dbState == null)
                    {

                        var dbState1 = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = dbState.state,
                            GUID_Parent = modelResult.StatesClass.GUID,
                            Type = globals.Type_Object,
                            New_Item = true
                        };
                        dbStates.Add(dbState1);
                    }
                    else
                    {
                        dbStates.Add(dbState.dbState);
                    }
                });

                var newStates = dbStates.Where(state => state.New_Item != null && state.New_Item.Value).ToList();

                if (newStates.Any())
                {
                    var dbWriter = new OntologyModDBConnector(globals);

                    result.ResultState = dbWriter.SaveObjects(newStates);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while writing the states";
                        return result;
                    }
                }

                result.Result.AddRange(dbStates);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetSyncIssuesModelResult>> GetSyncIssuesModel(SyncIssuesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetSyncIssuesModelResult>>(() =>
           {
               var result = new ResultItem<GetSyncIssuesModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetSyncIssuesModelResult()
               };

               if (string.IsNullOrEmpty(request.IdConfig) || !globals.is_GUID(request.IdConfig))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "IdConfig is not valid!";
                   return result;
               }

               if (string.IsNullOrEmpty(request.IdMasterUser) || !globals.is_GUID(request.IdMasterUser))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "IdMasterUser is not valid!";
                   return result;
               }

               if (string.IsNullOrEmpty(request.MasterPassword))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Masterpassword is not valid!";
                   return result;
               }

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Jira-Config!";
                   return result;
               }

               if (!dbReaderConfig.Objects1.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Jira-Config found!";
                   return result;
               }

               result.Result.ConfigItem = dbReaderConfig.Objects1.First();

               var searchUrl = dbReaderConfig.Objects1.Select(jiraConfig => new clsObjectRel
               {
                   ID_Object = jiraConfig.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_connect_to_Url.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_connect_to_Url.ID_Class_Right
               }).ToList();

               var dbReaderUrl = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderUrl.GetDataObjectRel(searchUrl);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Jira-Url!";
                   return result;
               }

               if (dbReaderUrl.ObjectRels.Count != 1)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No oder more than one Jira-Url found!";
                   return result;
               }

               result.Result.UrlItem = dbReaderUrl.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).First();

               var searchUser = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_authorized_by_user.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_authorized_by_user.ID_Class_Right
                   }
               };

               var dbReaderUser = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderUser.GetDataObjectRel(searchUser);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Jira-User!";
                   return result;
               }

               if (dbReaderUser.ObjectRels.Count != 1)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No or more than one Jira-User found!";
                   return result;
               }

               result.Result.UserItem = dbReaderUser.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).First();

               var searchIssueClass = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_belonging_Issue_Class.ID_RelationType
                   }
               };

               var dbReaderIssueClass = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderIssueClass.GetDataObjectRel(searchIssueClass);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Jira Issue-Class!";
                   return result;
               }


               if (dbReaderIssueClass.ObjectRels.Count != 1)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No or more than one Jira Issue-Class found!";
                   return result;
               }

               result.Result.IssueClass = dbReaderIssueClass.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).First();

               if (result.Result.IssueClass.Type != globals.Type_Class)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Jira-Class is no valid class!";
                   return result;
               }

               var searchTaskIdClass = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_belonging_Issue_Task_Class.ID_RelationType
                   }
               };

               var dbReaderTaskIdClass = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTaskIdClass.GetDataObjectRel(searchTaskIdClass);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Class for Task-Id!";
                   return result;
               }

               result.Result.TaskIdClass = dbReaderTaskIdClass.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type  = rel.Ontology
               }).FirstOrDefault();
        
               if (result.Result.TaskIdClass == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Task-Id Class found!";
                   return result;
               }

               if (result.Result.TaskIdClass.Type != globals.Type_Class)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Related Item is no Task-Id Class!";
                   return result;
               }

               var searchDirectionIssueToTaskId = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_Issue_Id_Relation_direction_Direction.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_Issue_Id_Relation_direction_Direction.ID_Class_Right
                   }
               };

               var dbReaderDirectionIssueToTaskId = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderDirectionIssueToTaskId.GetDataObjectRel(searchDirectionIssueToTaskId);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the direction for relation between Issue and Task-Id!";
                   return result;
               }

               result.Result.DirectionIssueTaskId = dbReaderDirectionIssueToTaskId.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.DirectionIssueTaskId == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No direction between issue and task-id found!";
                   return result;
               }

               var searchRelationTypeIssueTaskId = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.RelationType_belonging_RelationType.GUID
                   }
               };

               var dbReaderRelationTypeIssueTaskId = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRelationTypeIssueTaskId.GetDataObjectRel(searchRelationTypeIssueTaskId);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relationtype between Issue and Task-Id";
                   return result;
               }

               result.Result.RelationTypeIssueTaskId = dbReaderRelationTypeIssueTaskId.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.RelationTypeIssueTaskId == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Relationtype between Issue and Task-Id found!";
                   return result;
               }

               if (result.Result.RelationTypeIssueTaskId.Type != globals.Type_RelationType)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Item is no relationtype between Issue and Task-Id!";
                   return result;
               }

               var searchAssignee = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_assignee_user.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_assignee_user.ID_Class_Right
                   }
               };

               var dbReaderAssignee = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderAssignee.GetDataObjectRel(searchAssignee);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Assignee!";
                   return result;
               }

               result.Result.Assignees = dbReaderAssignee.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Assignees.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Assignee found!";
                   return result;
               }

               var searchStatesClass = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.RelationType_states_class.GUID
                   }
               };

               var dbReaderStatesClass = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderStatesClass.GetDataObjectRel(searchStatesClass);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the states-class";
                   return result;
               }

               result.Result.StatesClass = dbReaderStatesClass.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.Name_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.StatesClass == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No states-class found!";
                   return result;
               }

               if (result.Result.StatesClass.Type != globals.Type_Class)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You must provide a class for the states!";
                   return result;
               }

               var searchStatesClassRelationtype = new List<clsClassRel>
               {
                   new clsClassRel
                   {
                       ID_Class_Left = result.Result.IssueClass.GUID,
                       ID_Class_Right = result.Result.StatesClass.GUID
                   }
               };

               var dbReaderStatesClassRelationType = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderStatesClassRelationType.GetDataClassRel(searchStatesClassRelationtype);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relationtype between issue and state";
                   return result;
               }

               result.Result.StatesRelationType = dbReaderStatesClassRelationType.ClassRels.Select(clsRel => new clsOntologyItem
               {
                   GUID = clsRel.ID_RelationType,
                   Name = clsRel.Name_RelationType,
                   Type = globals.Type_RelationType
               }).FirstOrDefault();

               if (result.Result.StatesRelationType == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the relationtype between issue and state from classrelation";
                   return result;
               }

               if (result.Result.StatesRelationType.Type != globals.Type_RelationType)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The relationtype between issue and state from classrelation is no relationtype";
                   return result;
               }

               var searchProjectClass = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_project_class.ID_RelationType
                   }
               };

               var dbReaderProjectClass = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderProjectClass.GetDataObjectRel(searchProjectClass);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Project-Class";
                   return result;
               }

               result.Result.ProjectClass = dbReaderProjectClass.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.ProjectClass == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Projectclass found!";
                   return result;
               }

               if (result.Result.ProjectClass.Type != globals.Type_Class)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The related project-class is no class!";
                   return result;
               }

               var searchDirectionIssueProject = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_issue_to_project_direction_Direction.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_issue_to_project_direction_Direction.ID_Class_Right
                   }
               };

               var dbReaderIssueProjectDirection = new OntologyModDBConnector(globals);
               result.ResultState = dbReaderIssueProjectDirection.GetDataObjectRel(searchDirectionIssueProject);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the direction between issues and directions";
                   return result;
               }

               result.Result.DirectionProjectClass = dbReaderIssueProjectDirection.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.DirectionProjectClass == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The direction between issue and project cannot be found";
                   return result;
               }

               var searchProjectIssueRelationType = new List<clsClassRel>();

               if (result.Result.DirectionProjectClass.GUID == globals.Direction_LeftRight.GUID)
               {
                   searchProjectIssueRelationType = new List<clsClassRel>
                   {
                       new clsClassRel
                       {
                           ID_Class_Left = result.Result.IssueClass.GUID,
                           ID_Class_Right = result.Result.ProjectClass.GUID
                       }
                   };


               }
               else
               {
                   searchProjectIssueRelationType = new List<clsClassRel>
                   {
                       new clsClassRel
                       {
                           ID_Class_Right = result.Result.IssueClass.GUID,
                           ID_Class_Left = result.Result.ProjectClass.GUID
                       }
                   };
               }

               var dbReaderRelationTypeProjects = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRelationTypeProjects.GetDataClassRel(searchProjectIssueRelationType);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the class-relation between issues and projects!";
                   return result;
               }

               result.Result.ProjectRelationType = dbReaderRelationTypeProjects.ClassRels.Select(classRel => new clsOntologyItem
               {
                   GUID = classRel.ID_RelationType,
                   Name = classRel.Name_RelationType,
                   Type = globals.Type_RelationType
               }).FirstOrDefault();

               if (result.Result.ProjectRelationType == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No relationtype between issues and classes found!";
                   return result;

               }

               var searchCommitTagUser = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_commit_tagging_user.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_commit_tagging_user.ID_Class_Right
                   }
               };

               var dbReaderCommitTagUser = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderCommitTagUser.GetDataObjectRel(searchCommitTagUser);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error While getting the User for commmit-tagging";
                   return result;
               }

               result.Result.CommitTagUser = dbReaderCommitTagUser.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.CommitTagUser == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No User found for commit-tagging!";
                   return result;
               }

               var searchCommitTagGroup = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_JiraConnectorModule_commit_tagging_Group.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_JiraConnectorModule_commit_tagging_Group.ID_Class_Right
                   }
               };

               var dbReaderCommitTagGroup = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderCommitTagGroup.GetDataObjectRel(searchCommitTagGroup);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error While getting the Group for commmit-tagging";
                   return result;
               }

               result.Result.CommitTagGroup = dbReaderCommitTagGroup.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.CommitTagGroup == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Group found for commit-tagging!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
