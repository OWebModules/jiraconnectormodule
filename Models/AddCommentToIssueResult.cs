﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class AddCommentToIssueResult
    {
        public List<clsObjectRel> CommentItemToIssueList { get; set; } = new List<clsObjectRel>();
    }
}
