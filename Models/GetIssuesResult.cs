﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class GetIssuesResult
    {
        public List<clsOntologyItem> TaskIds { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> Issues { get; set; } = new List<clsObjectRel>();
    }
}
