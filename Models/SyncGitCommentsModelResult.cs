﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class SyncGitCommentsModelResult
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem JiraConfig { get; set; }
        public clsOntologyItem GitConfig { get; set; }

        public clsOntologyItem RegEx { get; set; }

        public clsObjectAtt RegExAttribute { get; set; }
    }
}
