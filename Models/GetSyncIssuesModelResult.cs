﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class GetSyncIssuesModelResult
    {
        public clsOntologyItem ConfigItem { get; set; }
        public clsOntologyItem UrlItem { get; set; }
        public clsOntologyItem UserItem { get; set; }
        public List<clsOntologyItem> Assignees { get; set; } = new List<clsOntologyItem>();
        public clsOntologyItem IssueClass { get; set; }
        public clsOntologyItem TaskIdClass { get; set; }
        public clsOntologyItem DirectionIssueTaskId { get; set; }
        public clsOntologyItem RelationTypeIssueTaskId { get; set; }
        public clsOntologyItem StatesClass { get; set; }
        public clsOntologyItem StatesRelationType { get; set; }
        public clsOntologyItem ProjectClass { get; set; }
        public clsOntologyItem DirectionProjectClass { get; set; }
        public clsOntologyItem ProjectRelationType { get; set; }
        public clsOntologyItem CommitTagUser { get; set; }
        public clsOntologyItem CommitTagGroup { get; set; }
    }
}
