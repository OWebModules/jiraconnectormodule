﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class AddCommentToIssueRequest
    {
        public List<clsOntologyItem> CommentItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> CommentMessages { get; set; } = new List<clsObjectAtt>();

        public string IdConfig { get; private set; }
        public string IdMasterUser { get; private set; }
        public string MasterPassword { get; private set; }

        public AddCommentToIssueRequest(string idConfig, 
            string idMasterUser, 
            string masterPassword, 
            List<clsOntologyItem> commentItems, 
            List<clsObjectAtt> commentMessages)
        {
            CommentItems = commentItems;
            CommentMessages = commentMessages;
            IdConfig = idConfig;
            IdMasterUser = idMasterUser;
            MasterPassword = masterPassword;
        }

    }
}
