﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class SyncIssuesRequest
    {
        public string IdConfig { get; private set; }
        public string IdMasterUser { get; private set; }
        public string MasterPassword { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public SyncIssuesRequest(string idConfig, string idMasterUser, string masterPassword)
        {
            IdConfig = idConfig;
            IdMasterUser = idMasterUser;
            MasterPassword = masterPassword;
        }
    }
}
