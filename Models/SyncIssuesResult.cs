﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Models
{
    public class SyncIssuesResult
    {
        public long CountFoundIssues { get; set; }
        public long CountCreatedJiraIssues { get; set; }
        public long CountCreatedOModulesIssues { get; set; }
    }
}
