﻿using Atlassian.Jira;
using GitConnectorModule.Models;
using JiraConnectorModule.Models;
using JiraConnectorModule.Services;
using JiraConnectorModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JiraConnectorModule
{
    public class JiraController : AppController
    {

        public async Task<ResultItem<AddCommentToIssueResult>> AddCommentToIssue(AddCommentToIssueRequest request)
        {
            var taskResult = await Task.Run<ResultItem<AddCommentToIssueResult>>(async() =>
           {
               var result = new ResultItem<AddCommentToIssueResult>
               {
                   ResultState = Globals.LState_Success.Clone()
               };

               var elasticAgent = new ElasticAgent(Globals);
               var typedTaggingConnector = new TypedTaggingModule.Connectors.TypedTaggingConnector(Globals);

               var modelRequest = new SyncIssuesRequest(request.IdConfig, request.IdMasterUser, request.MasterPassword);

               var modelResult = await elasticAgent.GetSyncIssuesModel(modelRequest);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               Uri jiraUir = null;
               if (!Uri.TryCreate(modelResult.Result.UrlItem.Name, UriKind.Absolute, out jiraUir))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The Jira-Url is not valid!";
                   return result;
               }

               var securityController = new SecurityController(Globals);

               var passwordResult = await securityController.GetPassword(modelResult.Result.UserItem, request.MasterPassword);

               result.ResultState = passwordResult.Result;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Password of the Jira-User!";
                   return result;
               }

               if (!passwordResult.CredentialItems.Any())
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Password found!";
                   return result;
               }

               var jiraPassword = passwordResult.CredentialItems.First().Password.Name_Other;


               try
               {
                   var jiraClient = Jira.CreateRestClient(modelResult.Result.UrlItem.Name, modelResult.Result.UserItem.Name, jiraPassword);
                   var commentsWithReference = request.CommentMessages.Where(comment => Regex.Match(comment.Val_String, @"#\d+").Success);
                   jiraClient.Issues.MaxIssuesPerRequest = 100000;
                   var issuesPre = jiraClient.Issues.Queryable.ToList().Where(issue => issue.Assignee != null && modelResult.Result.Assignees.Select(assignee => assignee.Name.ToLower()).Contains(issue.Assignee.ToLower())).ToList();
                   var getIssuesResult = await elasticAgent.GetIssues(issuesPre,
                       modelResult.Result.TaskIdClass.GUID,
                       modelResult.Result.IssueClass.GUID,
                       modelResult.Result.RelationTypeIssueTaskId.GUID,
                       modelResult.Result.DirectionIssueTaskId.GUID);

                   result.ResultState = getIssuesResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }


                   var issues = (from issue in issuesPre
                                 join dbTaskId in getIssuesResult.Result.TaskIds on issue.JiraIdentifier equals dbTaskId.Name
                                 join dbIssue in getIssuesResult.Result.Issues on dbTaskId.GUID equals (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? dbIssue.ID_Other : dbIssue.ID_Object)
                                 select new { issue, dbTaskId, dbIssue }).ToList();

                    var commentItems = (from comment in request.CommentItems
                     join commentMessage in request.CommentMessages on comment.GUID equals commentMessage.ID_Object
                     select new { comment, commentMessage }).ToList();

                   
                    foreach (var comment in commentItems)
                    {
                        var taskIdsMatches = Regex.Matches(comment.commentMessage.Val_String, @"#\d+").Cast<Match>().Select(taskMatch => taskMatch.Value.Replace("#", "")).ToList();
                        var issuesMatching = (from issue in issues
                                              join taskIdsMatch in taskIdsMatches on issue.issue.JiraIdentifier equals taskIdsMatch
                                              select issue).ToList();

                        foreach (var issue in issuesMatching)
                        {
                           var comments = (await issue.issue.GetCommentsAsync()).ToList();
                           if (comments == null || !comments.Any() || !comments.Any(com => com.Body.Contains(comment.comment.Name)))
                           {
                               var commentJiraItem = await issue.issue.AddCommentAsync($"Commit:{comment.comment.Name}, message: {comment.commentMessage.Val_String}");
                           }

                           var issueItem = new clsOntologyItem
                           {
                               GUID = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? issue.dbIssue.ID_Object : issue.dbIssue.ID_Other),
                               Name = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? issue.dbIssue.Name_Object : issue.dbIssue.Name_Other),
                               GUID_Parent = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? issue.dbIssue.ID_Parent_Object : issue.dbIssue.ID_Parent_Other),
                               Type = Globals.Type_Object
                           };

                           var typedTags = await typedTaggingConnector.GetTags(issueItem);

                           if (typedTags.Tags == null || !typedTags.Tags.Any(tag => tag.GUID == comment.comment.GUID))
                           {
                               var typedTaggingResult = await typedTaggingConnector.SaveTags(issueItem, new List<clsOntologyItem> { comment.comment }, modelResult.Result.CommitTagUser, modelResult.Result.CommitTagGroup, false, null);
                               result.ResultState = typedTaggingResult.Result;
                               if (result.ResultState.GUID == Globals.LState_Error.GUID)
                               {
                                   result.ResultState.Additional1 = "Error while saving typed Tag between issue and commit";
                               }
                           }
                            
                        }
                    }
                    

               }
               catch (Exception ex)
               {

                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while connect to Jira: {ex.Message}";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<AddCommentToIssueResult>> SyncGitComments(SyncGitCommentsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<AddCommentToIssueResult>>(async () =>
            {
                var result = new ResultItem<AddCommentToIssueResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new AddCommentToIssueResult()
                };

                result.ResultState = ValidationController.ValidateSyncGitCommentsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var elasticAgent = new ElasticAgent(Globals);

                var serviceResult = await elasticAgent.GetSyncGitCommentsModel(request);

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                Regex commitRegex = null;
                try
                {
                    commitRegex = new Regex(serviceResult.Result.RegExAttribute.Val_String);
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }

                var gitConnector = new GitConnectorModule.GitConnector(Globals);

                var syncGitProjectsRequest = new SyncGitProjectsRequest(serviceResult.Result.GitConfig.GUID)
                {
                    MessageOutput = request.MessageOutput
                };

                var gitResult = await gitConnector.SyncGitProjects(syncGitProjectsRequest);

                result.ResultState = gitResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var gitCommitsRequest = new GetGitCommitsRequest(serviceResult.Result.GitConfig.GUID)
                {
                    CommitTextFilterRegex = commitRegex,
                    MessageOutput = request.MessageOutput
                };

                var getGitCommitsResult = await gitConnector.GetGitCommits(gitCommitsRequest);

                result.ResultState = getGitCommitsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var addCommentToIssueRequest = new AddCommentToIssueRequest(serviceResult.Result.JiraConfig.GUID, request.IdMasterUser, request.MasterPassword, getGitCommitsResult.Result.GitCommitsToProjects.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = Globals.Type_Object
                }).ToList(), getGitCommitsResult.Result.CommitMessages);

                var addCommentToIssueResult = await AddCommentToIssue(addCommentToIssueRequest);

                result.ResultState = addCommentToIssueResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }


        public async Task<ResultItem<SyncIssuesResult>> SyncJiraIssues(SyncIssuesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncIssuesResult>>(async() =>
            {
                var result = new ResultItem<SyncIssuesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncIssuesResult()
                };

                var elasticAgent = new ElasticAgent(Globals);

                var modelResult = await elasticAgent.GetSyncIssuesModel(request);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                Uri jiraUir = null;
                if (!Uri.TryCreate(modelResult.Result.UrlItem.Name,UriKind.Absolute, out jiraUir))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Jira-Url is not valid!";
                    return result;
                }

                var securityController = new SecurityController(Globals);

                var passwordResult = await securityController.GetPassword(modelResult.Result.UserItem, request.MasterPassword);

                result.ResultState = passwordResult.Result;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Password of the Jira-User!";
                    return result;
                }

                if (!passwordResult.CredentialItems.Any())
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Password found!";
                    return result;
                }

                var jiraPassword = passwordResult.CredentialItems.First().Password.Name_Other;


                try
                {
                    var jiraClient = Jira.CreateRestClient(modelResult.Result.UrlItem.Name, modelResult.Result.UserItem.Name, jiraPassword);

                    var projectsFull = await jiraClient.Projects.GetProjectsAsync();
                    jiraClient.Issues.MaxIssuesPerRequest = 100000;
                    var issuesPre = jiraClient.Issues.Queryable.ToList();
                    var issues = issuesPre.Where(issue => issue.Assignee != null &&  modelResult.Result.Assignees.Select(assignee=> assignee.Name.ToLower()).Contains(issue.Assignee.ToLower())).ToList();
                    result.Result.CountFoundIssues = issues.Count;

                    var taskIds = issues.Select(issue => issue.JiraIdentifier).ToList();

                    var taskIdResult = await elasticAgent.CheckObjects(taskIds, modelResult.Result.TaskIdClass.GUID, modelResult.Result);

                    result.ResultState = taskIdResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    var projects = (from project in projectsFull
                                    join key in issues.GroupBy(issue => issue.Project).Select(issueGroup => issueGroup.Key) on project.Key equals key
                                    select project.Name).ToList();

                    var checkProjectsResult = await elasticAgent.CheckObjects(projects, modelResult.Result.ProjectClass.GUID, modelResult.Result);
                    result.ResultState = checkProjectsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }                   

                    var taskIdsToIssues = (from taskId in taskIdResult.Result
                                           join issue in issues on taskId.Name equals issue.JiraIdentifier
                                           select new { taskId, issue }).ToList();
                    taskIdsToIssues.ForEach(issue =>
                     {
                         issue.taskId.Additional1 = issue.issue.Summary;
                     });

                    var issueResult = await elasticAgent.CheckIssuesOfIdentifiers(taskIdResult.Result, modelResult.Result);
                    result.ResultState = issueResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var taskStates = issues.GroupBy(issue => issue.Status.Name).Select(state => state.Key).ToList();

                    var statesResult = await elasticAgent.CheckStates(taskStates, modelResult.Result);

                    result.ResultState = statesResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var relationConfig = new clsRelationConfig(Globals);

                    if (modelResult.Result.DirectionProjectClass.GUID == Globals.Direction_LeftRight.GUID)
                    {
                        var issuesToProjects = new List<clsObjectRel>();
                        var issuesToProjectsPre = (from taskIdToIssue in taskIdsToIssues
                                                   join issue in issueResult.Result on taskIdToIssue.issue.JiraIdentifier equals (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? issue.Name_Other : issue.Name_Object)
                                                   join projectFull in projectsFull on taskIdToIssue.issue.Project equals projectFull.Key
                                                   join project in checkProjectsResult.Result on projectFull.Name.ToLower() equals project.Name.ToLower()
                                                   select new { issue, project, projectFull }).ToList();
                        var orderId = 1;
                        for (var i = issues.Count - 1; i >= 0; i--)
                        {
                            var issue = issues[i];
                            var relPre = issuesToProjectsPre.First(rel1 => rel1.projectFull.Key == issue.Project && (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? rel1.issue.Name_Other : rel1.issue.Name_Object) == issue.JiraIdentifier);
                            var rel = relationConfig.Rel_ObjectRelation(new clsOntologyItem
                            {
                                GUID = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? relPre.issue.ID_Object : relPre.issue.ID_Other),
                                Name = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? relPre.issue.Name_Object : relPre.issue.Name_Other),
                                GUID_Parent = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? relPre.issue.ID_Parent_Object : relPre.issue.ID_Parent_Other),
                                Type = Globals.Type_Object
                            },
                            relPre.project,
                            modelResult.Result.ProjectRelationType, false, orderId ++);
                            issuesToProjects.Add(rel);
                        }

                        var checkIssuesToProjects = await elasticAgent.CheckRelations(issuesToProjects, modelResult.Result.ProjectClass.GUID, modelResult.Result.DirectionProjectClass.GUID, false);

                        result.ResultState = checkIssuesToProjects.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }
                    else
                    {
                        var issuesToProjects = new List<clsObjectRel>();
                        var issuesToProjectsPre = (from taskIdToIssue in taskIdsToIssues
                                                   join issue in issueResult.Result on taskIdToIssue.issue.JiraIdentifier equals (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? issue.Name_Other : issue.Name_Object)
                                                   join projectFull in projectsFull on taskIdToIssue.issue.Project equals projectFull.Key
                                                   join project in checkProjectsResult.Result on projectFull.Name.ToLower() equals project.Name.ToLower()

                                                   select new { issue, project, projectFull}).ToList();

                        var orderId = 1;
                        for (var i = issues.Count-1; i >= 0; i--)
                        {
                            var issue = issues[i];
                            var relPre = issuesToProjectsPre.First(rel1 => rel1.projectFull.Key == issue.Project && (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? rel1.issue.Name_Other : rel1.issue.Name_Object) == issue.JiraIdentifier);
                            var rel = relationConfig.Rel_ObjectRelation(relPre.project, new clsOntologyItem
                            {
                                GUID = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? relPre.issue.ID_Object : relPre.issue.ID_Other),
                                Name = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? relPre.issue.Name_Object : relPre.issue.Name_Other),
                                GUID_Parent = (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID ? relPre.issue.ID_Parent_Object : relPre.issue.ID_Parent_Other),
                                Type = Globals.Type_Object
                            },
                            modelResult.Result.ProjectRelationType, false, orderId ++);
                            issuesToProjects.Add(rel);
                        }

                        var checkIssuesToProjects = await elasticAgent.CheckRelations(issuesToProjects, modelResult.Result.ProjectClass.GUID, modelResult.Result.DirectionProjectClass.GUID, false);

                        result.ResultState = checkIssuesToProjects.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                    if (modelResult.Result.DirectionIssueTaskId.GUID == Globals.Direction_LeftRight.GUID)
                    {
                        

                        var issuesToStates = (from taskIdToIssue in taskIdsToIssues
                                              join issue in issueResult.Result on taskIdToIssue.issue.JiraIdentifier equals issue.Name_Other
                                              join state in statesResult.Result on taskIdToIssue.issue.Status.Name.ToLower() equals state.Name.ToLower()
                                              select new { issue, state }).ToList();

                        var issueStates = issuesToStates.Select(rel => relationConfig.Rel_ObjectRelation(new clsOntologyItem
                        {
                            GUID = rel.issue.ID_Object,
                            Name = rel.issue.Name_Object,
                            GUID_Parent = rel.issue.ID_Parent_Object,
                            Type = Globals.Type_Object
                        },
                        rel.state,
                        modelResult.Result.StatesRelationType)).ToList();

                        var resultSaveRelations = await elasticAgent.CheckRelations(issueStates, modelResult.Result.IssueClass.GUID, Globals.Direction_LeftRight.GUID, true);

                        result.ResultState = resultSaveRelations.ResultState;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }
                    else
                    {
                        var issuesToStates = (from taskIdToIssue in taskIdsToIssues
                                              join issue in issueResult.Result on taskIdToIssue.issue.JiraIdentifier equals issue.Name_Object
                                              join state in statesResult.Result on taskIdToIssue.issue.Status.Name.ToLower() equals state.Name.ToLower()
                                              select new { issue, state }).ToList();

                        var issueStates = issuesToStates.Select(rel => relationConfig.Rel_ObjectRelation(new clsOntologyItem
                        {
                            GUID = rel.issue.ID_Other,
                            Name = rel.issue.Name_Other,
                            GUID_Parent = rel.issue.ID_Parent_Other,
                            Type = Globals.Type_Object
                        },
                        rel.state,
                        modelResult.Result.StatesRelationType)).ToList();

                        var resultSaveRelations = await elasticAgent.CheckRelations(issueStates, modelResult.Result.IssueClass.GUID, Globals.Direction_LeftRight.GUID, true);

                        result.ResultState = resultSaveRelations.ResultState;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                    //var issues = (from issue in await jiraClient.Issues.GetIssuesAsync()
                    //              where issue.Assignee.ToLower() == modelResult.Result.UserItem.Name
                    //                select issue).ToList();

                    //var taskIds = issues.Select(issue => issue.JiraIdentifier).ToList();



                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while connect to Jira: {ex.Message}";
                    return result;
                }
                

                return result;
            });

            return taskResult;
        }

        public JiraController(Globals globals) : base(globals)
        {
        }
    }

}
