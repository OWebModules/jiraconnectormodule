﻿using JiraConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorModule.Validation
{
    public class ValidationController
    {
        public static clsOntologyItem ValidateSyncGitCommentsRequest(SyncGitCommentsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(request.IdMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdMasterUser is invalid!";
                return result;
            }

            if (!globals.is_GUID(request.IdMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdMasterUser is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(request.MasterPassword))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Masterpassword is invalid!";
                return result;
            }
            return result;
        }
    }
}
